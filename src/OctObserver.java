class OctObserver extends Observer { 
  public OctObserver( Subject s ) {
    subj = s;
    subj.attach( this );
  } 
  public void update(View view) {
    System.out.print( " " + Integer.toOctalString( subj.getState() ) );
  } 
}