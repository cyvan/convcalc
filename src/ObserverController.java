public class ObserverController extends PatternStarter {

  public void execute(View view) {
	  
    Subject sub = new Subject();
    
    // Client configures the number and type of Observers
    new HexObserver( sub );
    new OctObserver( sub );
    new BinObserver( sub );
    
    //scanner for input
    String s = null;
    int num = 0;
    boolean running = true;
    
    //keep running till false
    while (running == true) {
      
      s = view.displayMSGgetInput( "Enter number please or quit to stop: " );
      
      if(s.toLowerCase().equals("quit")){
    	  
    	  running = false;
    	  
      } else {

    	  try{
    		  num = Integer.parseInt(s);
    	  }
    	  catch(NumberFormatException ex){
    		  view.displayMSG("Its not a valid Integer");
    	  }
      
    	  sub.setState( num, view );
    	  view.displayMSG("\n");
      
      }
    }
  }
}