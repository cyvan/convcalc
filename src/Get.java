import java.util.Scanner;

public class Get implements Strategy{
	
	@Override
	public void Do(int first, int second){
			Scanner reader = new Scanner(System.in);
			System.out.println("To Add type a, to substract type s, to multyply type m or to devide type d:");
			//get user input for a
			String choise=reader.next();
			String input = choise.toLowerCase();
			if(input!=null){
				Solve solve;
				switch(input){
				case("a"): solve = new Add();
					break;
				case("m"): solve = new Multiply();
					break;
				case("s"): solve = new Substract();
					break;
				case("d"): solve  = new Devide();
					break;
				default: solve = new Add(); 
				}
				solve.SolveIt(first, second);
			}
			else{
				System.out.println("you did not type anything");
			}
				
		}

	@Override
	public void number() {
		// TODO Auto-generated method stub
		
	}
}
