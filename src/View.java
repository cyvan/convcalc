import java.util.Scanner;


public class View {
	
	//Constructor called upon declaration to create the view
	public View(){
		
		
		
	}
	
	/*///////////////////////////////////////////////////////////
	 * functions used to display messages on screen and get input
	 *///////////////////////////////////////////////////////////
	
	//function used to display a message
	public void displayMSG(String msg){
		System.out.println(msg);
	}

	
	//function used to display a message and get input
	public String displayMSGgetInput(String msg){
		System.out.println(msg);
		
		@SuppressWarnings("resource")
		Scanner scanMain = new Scanner(System.in);
		String sMain = null;
		String Main = scanMain.next();
		sMain = Main.toLowerCase();
		
		return sMain;
	}

	
	//function used to display a message and get input
	public String GgetInput(){
		
		@SuppressWarnings("resource")
		Scanner scanMain = new Scanner(System.in);
		String sMain = null;
		String Main = scanMain.next();
		sMain = Main.toLowerCase();
		
		return sMain;
	}

}
