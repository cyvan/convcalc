
@SuppressWarnings("unused")
public class Model {
	
	//Private values to store values in the Model class
	private String hex;
	private String oct;
	private String bin;
	
	//Setters to set the values in the Model class
	public void setHex(String hex){
		this.hex = hex;
	}

	public void setOct(String oct){
		this.oct = oct;
	}

	public void setBin(String bin){
		this.bin = bin;
	}
	
	//Getters to get the values form the Model Class
	public String getHex(String hex){
		 return hex;
	}

	public String getOct(String oct){
		return oct;
	}

	public String getBin(String bin){
		return bin;
	}
	

}
