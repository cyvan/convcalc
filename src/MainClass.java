/**
* Geeft de naam van de persoon
* @author Cyvan Oort
* @param id 5
* @since 04-02-2014
* @version 1.0
*/

public class MainClass {
	
	@SuppressWarnings("unused")
	public static void main(String[] args){
	
		View view = new View();
		Model model = new Model();
	
		Controller controller = new Controller(view, model);
		
	}

}
