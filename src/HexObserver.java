class HexObserver extends Observer { 
  public HexObserver( Subject s ) { 
    subj = s; 
    subj.attach( this ); 
  }

  public void update(View view) { 
	  view.displayMSG( " " + Integer.toHexString( subj.getState() ) ); 
  } 
}