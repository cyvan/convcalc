
class Subject { 
	
  //variables
  private Observer[] observers = new Observer[9];
  private int totalObs = 0;
  private int state;
  public void attach( Observer o ) {
    observers[totalObs++] = o;
    
  }

  //get state of subject
  public int getState() {
    return state;
  }
  
  //set state of subject
  public void setState( int in, View view ) {
    state = in;
    Notify(view);
  }

  //notify everything
  private void Notify(View view) {
	  
    for (int i=0; i < totalObs; i++) {
      observers[i].update(view);
    }
    
  }
  
}