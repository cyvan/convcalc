
class BinObserver extends Observer { 
	
  public BinObserver( Subject s ) { 
	  
    subj = s; 
    subj.attach( this ); } // Observers register themselves 
  
    public void update(View view) { 
    	
    	view.displayMSG( " " + Integer.toBinaryString( subj.getState() )); 
    
  } 
    
}