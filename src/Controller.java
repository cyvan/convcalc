import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Controller {
	
	//model and view associated with the controller
	private View view;
	private Model model;
	
	//constructor called upon declaration to pass in model and view
	public Controller(View view, Model model){
		
		this.view = view;
		this.model = model;
		
		while(true){

		   List queue = produceRequests();
		   workOffRequests( queue );
		}
		
	}
	
	
	interface Command { void execute(); }

	class Calculator implements Command {
	   public void execute() {
	      
		   PatternStarter pat;
		   Factory factory =  new StrategyFactory();
		   pat = factory.makeChoice();
		   pat.execute();
	   }  
	}

	class Converter implements Command {
	   public void execute() {
	      
		   PatternStarter pat;
		   Factory factory = new ObserverFactory();
		   pat = factory.makeChoice();
		   pat.execute();
		   
	   }  
	}
	

	public List produceRequests() {
	   List queue = new ArrayList();

	   String sMain = view.displayMSGgetInput("Please enter a command(Conv/Calc):").toLowerCase();;
	   if(sMain.equals("conv")) {
		   
		   queue.add( new Converter() );
		   
	   } else if(sMain.equals("calc")) {
		   
		   queue.add( new Calculator() );
		   
	   }else{
		   
		   view.displayMSG("Invalid command");
		   
	   }
	   
	   return queue;
	}

	public void workOffRequests( List queue ) {
	   for (Iterator it = queue.iterator(); it.hasNext(); ){
	      ((Command)it.next()).execute();
	   }
	}	

}
